# Graphics Coursework 2: Drone Simulator
This is my Graphics 2 Coursework, it is a simple drone simulator where the 3D drone is free to roam around the environment, with some basic collision detection in use.


## Prerequisites
To make this project work, you'll need:

* Visual Studio

## Controls
To move the drone, here are the controls:

* Space - Elevate the Drone
* Shift - Lower the Drone
* W - Move the drone forward
* S - Move the drone backward
* A - Tilt the drone to the left
* D - Tilt the drone to the right
* Right Arrow - Rotate the drone to the right
* Left Arrow - Rotate the drone to the left

###Camera Controls
There are also multiple views of the drone which can be used. To change the camera use these keys:

* 1 - Default 3rd person camera
* 2 - LookAt/Drone controller perspective camera
* 3 - Drone perspective camera

#Screenshots of the Project
Here are some screenshots of the project

![image1](Screenshots/firstperson.png)
![image2](Screenshots/dronehb.png)
![image3](Screenshots/lookAt.png)
![image4](Screenshots/tiltfront.png)
![image5](Screenshots/tiltleft.png)
![image6](Screenshots/reset.png)


