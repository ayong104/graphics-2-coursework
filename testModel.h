#pragma once
#include "LightingEngine.h"
#include "shaders\Shader.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"


//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"





//
class testModel
{
public:
	//model
	glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing
	glm::mat4 ProjectionMatrix; // matrix for the orthographic projection

	
	OBJLoader objLoader;
	//glm::vec3 positionVector;
	ThreeDModel model, modelbox;
	LightingEngine light;
	//methods
	testModel();

	float b1MinX;
	float b1MaxX;
	float b1MinY;
	float b1MaxY;
	float b1MinZ;
	float b1MaxZ;

	void draw(glm::vec3 positionVector, glm::mat4 viewingMatrix, Shader* myShader);
	void loadModel(char* filename, Shader * myShader);
};