#include "testModel.h"
#include "drone.h"


testModel::testModel()
{

}

void testModel::draw(glm::vec3 positionVector, glm::mat4 viewingMatrix, Shader* myShader)
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	//glUseProgram(myShader->handle());  // use the shader
	float material_shine[4] = {0.1f,0.1f,0.1f,1.0f};
	float Material_Ambient[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
	float Material_Diffuse[4] = { 0.8f, 0.8f, 0.5f, 1.0f };
	float Light_Ambient_And_Diffuse[4] = { 0.5f, 0.5f, 0.4f, 0.7f };

	/*glm::mat4 viewingMatrix = glm::translate(glm::mat4(1.0), glm::vec3(0, 0, -70));*/
	//glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewingMatrix[0][0]); //view matrix 
	light.loadShader(Material_Diffuse, Light_Ambient_And_Diffuse, Material_Ambient, material_shine, 2, myShader);

	//DRAW THE MODEL



	//ModelViewMatrix = glm::mat4(1.0f);
	//positionVector = glm::vec3(-50, 6, 0);

	

	ModelViewMatrix = glm::translate(viewingMatrix, positionVector);
	ModelViewMatrix = glm::rotate(ModelViewMatrix, 90.0f, glm::vec3(1, 0, 0));//simple transform/rotation to make the models orient in the right position.
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	//camera movement
	//viewingMatrix = glm::translate(ModelViewMatrix, glm::vec3(moveX, moveY, moveZ));

	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	//glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	model.drawElementsUsingVBO(myShader);
	//model.drawBoundingBox(myShader);

	b1MinX = model.octree->minX + positionVector.x;
	b1MaxX = model.octree->maxX + positionVector.x;
	b1MinY = model.octree->minY + positionVector.y;
	b1MaxY = model.octree->maxY + positionVector.y;
	b1MinZ = model.octree->minZ + positionVector.z;
	b1MaxZ = model.octree->maxZ + positionVector.z;

	glFlush();




}
void testModel::loadModel(char* filename, Shader* myShader)
{


	if (objLoader.loadModel("models/building/building.obj", model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		model.calcCentrePoint();
		model.centreOnZero();


		model.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
		model.initDrawElements();					//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher 
		model.initVBO(myShader);
		model.deleteVertexFaceData();

	}
	else
	{
		cout << " model failed to load " << endl;
	}
}



