#pragma once
#include "shaders\Shader.h"

#include <iostream>



//extern Shader* myShader;  ///shader object 
//float amount = 0;
//float temp = 0.002f;


class LightingEngine
{
private:
	//float Material_Ambient[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
	//float Material_Diffuse[4] = { 0.8f, 0.8f, 0.5f, 1.0f };
	//float Material_Specular[4] = { 0.9f,0.9f,0.8f,1.0f };
	//float Material_Shininess = 50;

	//Light Properties
	//float Light_Ambient_And_Diffuse[4] = { 0.8f, 0.8f, 0.6f, 1.0f };
	float Light_Specular[4] = { 1.0f,1.0f,1.0f,1.0f };
	float LightPos[4] = { 0.0f, 20000.0f, 1.0f, 0.0f };

public:
	//Material properties
	LightingEngine();
	void loadShader(float Material_Diffuse[4], float Light_Ambient_And_Diffuse[4], float Material_Ambient[4],
		float Material_Specular[4], float Material_Shininess, Shader * myShader);
	

};