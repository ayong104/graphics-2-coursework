#include "projFloor.h"



projFloor::projFloor()
{}

void projFloor::draw(glm::mat4 viewingMatrix, Shader* myShader)
{
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	//glUseProgram(myShader->handle());  // use the shader
	float material_shine[4] = { 0.2f,0.2f,0.2f,0.5f };
	float Material_Ambient[4] = { 0.05f, 0.05f, 0.05f, 0.5f };
	float Material_Diffuse[4] = { 0.8f, 0.8f, 0.5f, 1.0f };
	float Light_Ambient_And_Diffuse[4] = { 0.6f, 0.6f, 0.3f, 1.0f };

	/*glm::mat4 viewingMatrix = glm::translate(glm::mat4(1.0), glm::vec3(0, 0, -70));*/
	//glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewingMatrix[0][0]); //view matrix 
	light.loadShader(Material_Diffuse, Light_Ambient_And_Diffuse, Material_Ambient, material_shine, 1, myShader);

	//DRAW THE MODEL



	//ModelViewMatrix = glm::mat4(1.0f);




	ModelViewMatrix = glm::translate(viewingMatrix, glm::vec3(0, 0, 0));
	ModelViewMatrix = glm::rotate(ModelViewMatrix, 90.0f, glm::vec3(1, 0, 0));//simple transform/rotation to make the models orient in the right position.
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	
	
	



	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	//glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ProjectionMatrix"), 1, GL_FALSE, &ProjectionMatrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);

	model.drawElementsUsingVBO(myShader);
	//model.drawOctreeLeaves(myShader);
	glFlush();




}
void projFloor::loadModel(char* filename, Shader* myShader)
{


	if (objLoader.loadModel(filename, model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		model.calcCentrePoint();
		model.centreOnZero();


		model.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.
		model.initDrawElements();					//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher 
		model.initVBO(myShader);
		model.deleteVertexFaceData();

	}
	else
	{
		cout << " model failed to load " << endl;
	}
}



