//OPENGL 3.2 DEMO FOR RENDERING OBJECTS LOADED FROM OBJ FILES

//includes areas for keyboard control, mouse control, resizing the window
//and draws a spinning rectangle

#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib,"glew32.lib")

#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL


#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"
#include "Drone.h"
#include "testModel.h"
#include "projFloor.h"

#include <math.h>
using namespace std;
//shader

Shader* myShader;  ///shader object 
//Shader* myBasicShader;

glm::mat4 ProjectionMatrix; // matrix for the orthographic projection


//math
const float PI = 3.1415926535897932384626433832795f;

float cameraX;
float cameraY;
float cameraZ;

//time
double deltaT = 0;
__int64 prevTime = 0;
double timerFreqRecip = 0.000003;


//models
Drone drone;
testModel building1;
testModel building2;
testModel building3;
testModel building4;
testModel building5;
testModel building6;
testModel building7;
testModel building8;
projFloor plane;

ThreeDModel model, modelbox;
OBJLoader objLoader;

//keys
int	mouse_x=0, mouse_y=0;
bool LeftPressed = false;
int screenWidth=600, screenHeight=600;
bool keys[256];
float spin=180;
int cameraState =1;

glm::vec3 currentPos;
glm::vec3 prevPos;
glm::vec3 deltaPos;

//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void reshape(int width, int height);				//called when the window is resized
void init();				//called in winmain when the program starts.
void update();				//called in winmain to update variables
void processKeys();         //called in winmain to process keyboard input


/*************    START OF OPENGL FUNCTIONS   ****************/
void display()									
{
	//camera
	GLuint matLocation = glGetUniformLocation(myShader->handle(), "ProjectionMatrix");
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	

	glm::mat4 viewingMatrix = glm::mat4(1.0f);

	
	if (cameraState == 1) //normal POV
	{
		viewingMatrix = glm::translate(viewingMatrix, glm::vec3(0, -7.5f, -30));
		viewingMatrix = glm::rotate(viewingMatrix, drone.rotateZ, glm::vec3(0, 1, 0));//rotate by y
		viewingMatrix = glm::translate(viewingMatrix, glm::vec3(-drone.moveX, -drone.moveY , -drone.moveZ));
	}
	else if (cameraState == 2) //lookAt floor POV
	{
		viewingMatrix = glm::lookAt(glm::vec3(0,3,0), glm::vec3(drone.moveX, drone.moveY, drone.moveZ), glm::vec3(0,1,0));
		
	}
	else if (cameraState == 3) //drone POV
	{
		viewingMatrix = glm::translate(viewingMatrix, glm::vec3(0, 3.0f, -3.0f));
		viewingMatrix = glm::rotate(viewingMatrix, drone.rotateZ, glm::vec3(0, 1, 0));//rotate by y
		viewingMatrix = glm::translate(viewingMatrix, glm::vec3(-drone.moveX, -drone.moveY, -drone.moveZ));
		
	}
	
	


	//viewingMatrix = glm::rotate(viewingMatrix, -drone.rotateX, glm::vec3(1, 0, 0));//rotate by x
	//viewingMatrix = glm::rotate(viewingMatrix, -drone.rotateY, glm::vec3(0, 1, 0));//rotate by y

		
	//viewingMatrix = glm::rotate(viewingMatrix, -drone.rotateZ, glm::vec3(0, 0, 1));//rotate by z


	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewingMatrix[0][0]); //view matrix


	drone.draw(viewingMatrix, myShader); //draws drone
	building1.draw(glm::vec3(-50, 6, 0), viewingMatrix, myShader); //draws building
	building2.draw(glm::vec3(50, 6, 0), viewingMatrix, myShader); //draws building 
	building3.draw(glm::vec3(-50, 6, -100), viewingMatrix, myShader); //draws building
	building4.draw(glm::vec3(50, 6, -100), viewingMatrix, myShader); //draws building 
	building5.draw(glm::vec3(-50, 6, -200), viewingMatrix, myShader); //draws building
	building6.draw(glm::vec3(50, 6, -200), viewingMatrix, myShader); //draws building 
	building7.draw(glm::vec3(-50, 6, 100), viewingMatrix, myShader); //draws building
	building8.draw(glm::vec3(50, 6, 100), viewingMatrix, myShader); //draws building 
	plane.draw(viewingMatrix, myShader); //draws plane


	/*cout << "prev x: " << prevPos.x << endl;
	cout << "prev y: " << prevPos.y << endl;
	cout << "prev z: " << prevPos.z << endl;*/

	/*cout << "current x: " << currentPos.x << endl;
	cout << "current y: " << currentPos.y << endl;
	cout << "current z: " << currentPos.z << endl;
*/
	/*cout << "delta x: " << deltaPos.x << endl;
	cout << "delta y: " << deltaPos.y << endl;
	cout << "delta z: " << deltaPos.z << endl;*/

	/*cout << "Is Colliding: " << drone.isCollide << endl;*/

	
	/*cout << "Building bounding box minX: " << building1.b1MinX << endl;
	cout << "Building bounding box maxX: " << building1.b1MaxX << endl;
	cout << "Building bounding box minY: " << building1.b1MinY << endl;
	cout << "Building bounding box maxY: " << building1.b1MaxY << endl;
	cout << "Building bounding box minZ: " << building1.b1MinZ << endl;
	cout << "Building bounding box maxZ: " << building1.b1MaxZ << endl;*/

}

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset The Current Viewport

	//Set the projection matrix
	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 1.0f, 1000.0f);
	
	
}
void init()
{
	glClearColor(0.25,0.5,0.75,0.0);						//sets the clear colour to yellow
														//glClear(GL_COLOR_BUFFER_BIT) in the display function
														//will clear the buffer to this colour
	glEnable(GL_DEPTH_TEST);

	myShader = new Shader;
	if (!myShader->load("BasicView", "glslfiles/basicTransformations.vert", "glslfiles/basicTransformations.frag"))
	{
		std::cout << "failed to load shader" << std::endl;
	}
	
	glUseProgram(myShader->handle());
	glEnable(GL_TEXTURE_2D);
	
	cout << " loading model " << endl;
	drone.loadModel("models/drone/drone.obj", myShader);

	building1.loadModel("models/building/building.obj", myShader);
	building2.loadModel("models/building/building.obj", myShader);
	building3.loadModel("models/building/building.obj", myShader);
	building4.loadModel("models/building/building.obj", myShader);
	building5.loadModel("models/building/building.obj", myShader);
	building6.loadModel("models/building/building.obj", myShader);
	building7.loadModel("models/building/building.obj", myShader);
	building8.loadModel("models/building/building.obj", myShader);

	plane.loadModel("models/plane/plane.obj", myShader);

	
	
	
	
}
void processKeys()
{

	//rotating keys
	if(keys[VK_UP])
	{
		//drone.rotateX += -drone.speed;
	}
	if(keys[VK_DOWN])
	{
		//drone.rotateX += drone.speed;
	}
	if(keys[VK_LEFT])
	{
		drone.rotateZ += -drone.speed * 2;
		if (drone.rotateZ <= -360.0f)
		{
			drone.rotateZ = 0;
		}
	}
	if (keys[VK_RIGHT])
	{
		drone.rotateZ += drone.speed * 2;
		if (drone.rotateZ >= 360.0f)
		{
			drone.rotateZ = 0;
		}
	}

	//drone collision response
	if (drone.isCollide && drone.droneMinY >= 0)
	{
		//x movement
		if (building1.b1MinX <= drone.droneMinX && drone.droneMinX <= building1.b1MaxX ||
			building2.b1MinX <= drone.droneMinX && drone.droneMinX <= building2.b1MaxX ||
			building3.b1MinX <= drone.droneMinX && drone.droneMinX <= building3.b1MaxX ||
			building4.b1MinX <= drone.droneMinX && drone.droneMinX <= building4.b1MaxX ||
			building5.b1MinX <= drone.droneMinX && drone.droneMinX <= building5.b1MaxX ||
			building6.b1MinX <= drone.droneMinX && drone.droneMinX <= building6.b1MaxX ||
			building7.b1MinX <= drone.droneMinX && drone.droneMinX <= building7.b1MaxX ||
			building8.b1MinX <= drone.droneMinX && drone.droneMinX <= building8.b1MaxX) //left
		{
			drone.moveX -= deltaPos.x * 2;
			drone.moveY -= deltaPos.y * 2;
			drone.moveZ -= deltaPos.z * 2;

			/*drone.moveX += drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveZ += drone.speed * sinf(drone.rotateZ * PI / 180);*/
			//cout << "Moving Right" << endl;
		}
		else if (building1.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building1.b1MaxX ||
					building2.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building2.b1MaxX ||
					building3.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building3.b1MaxX ||
					building4.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building4.b1MaxX ||
					building5.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building5.b1MaxX ||
					building6.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building6.b1MaxX ||
					building7.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building7.b1MaxX ||
					building8.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building8.b1MaxX) //right
		{
			drone.moveX -= deltaPos.x * 2;
			drone.moveY -= deltaPos.y * 2;
			drone.moveZ -= deltaPos.z * 2;

			/*drone.moveX -= drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveZ -= drone.speed * sinf(drone.rotateZ * PI / 180);*/
			//cout << "Moving Left" << endl;
		}
		else if (building1.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building1.b1MaxZ ||
				building2.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building2.b1MaxZ ||
				building3.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building3.b1MaxZ ||
				building4.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building4.b1MaxZ ||
				building5.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building5.b1MaxZ ||
				building6.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building6.b1MaxZ ||
				building7.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building7.b1MaxZ ||
				building8.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building8.b1MaxZ)//front
		{
			drone.moveX -= deltaPos.x * 2;
			drone.moveY -= deltaPos.y * 2;
			drone.moveZ -= deltaPos.z * 2;

			/*drone.moveZ += drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveX -= drone.speed * sinf(drone.rotateZ * PI / 180);*/
			//cout << "Moving back" << endl;
		}
		else if (building1.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building1.b1MaxZ ||
				building2.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building2.b1MaxX ||
				building3.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building3.b1MaxX ||
				building4.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building4.b1MaxX ||
				building5.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building5.b1MaxX ||
				building6.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building6.b1MaxX ||
				building7.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building7.b1MaxX ||
				building8.b1MinZ <= drone.droneMinZ && drone.droneMinZ <= building8.b1MaxX)//back
		{
			drone.moveX -= deltaPos.x * 2;
			drone.moveY -= deltaPos.y * 2;
			drone.moveZ -= deltaPos.z * 2;

			/*drone.moveZ -= drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveX += drone.speed * sinf(drone.rotateZ * PI / 180);*/
			//cout << "Moving forward" << endl;
		}
		else if ((drone.droneMinY <= building1.b1MaxY) && 
				(building1.b1MinX <= drone.droneMinX && drone.droneMinX <= building1.b1MaxX) &&
				(building1.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building1.b1MaxX) &&
				(building1.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building1.b1MaxZ) &&
				(building1.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building1.b1MaxZ) ||

			(drone.droneMinY <= building2.b1MaxY) &&
			(building2.b1MinX <= drone.droneMinX && drone.droneMinX <= building2.b1MaxX) &&
			(building2.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building2.b1MaxX) &&
			(building2.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building2.b1MaxZ) &&
			(building2.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building2.b1MaxZ) ||

			(drone.droneMinY <= building3.b1MaxY) &&
			(building3.b1MinX <= drone.droneMinX && drone.droneMinX <= building3.b1MaxX) &&
			(building3.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building3.b1MaxX) &&
			(building3.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building3.b1MaxZ) &&
			(building3.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building3.b1MaxZ) ||

			(drone.droneMinY <= building4.b1MaxY) &&
			(building4.b1MinX <= drone.droneMinX && drone.droneMinX <= building4.b1MaxX) &&
			(building4.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building4.b1MaxX) &&
			(building4.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building4.b1MaxZ) &&
			(building4.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building4.b1MaxZ) ||

			(drone.droneMinY <= building5.b1MaxY) &&
			(building5.b1MinX <= drone.droneMinX && drone.droneMinX <= building5.b1MaxX) &&
			(building5.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building5.b1MaxX) &&
			(building5.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building5.b1MaxZ) &&
			(building5.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building5.b1MaxZ) ||

			(drone.droneMinY <= building6.b1MaxY) &&
			(building6.b1MinX <= drone.droneMinX && drone.droneMinX <= building6.b1MaxX) &&
			(building6.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building6.b1MaxX) &&
			(building6.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building6.b1MaxZ) &&
			(building6.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building6.b1MaxZ) ||

			(drone.droneMinY <= building7.b1MaxY) &&
			(building7.b1MinX <= drone.droneMinX && drone.droneMinX <= building7.b1MaxX) &&
			(building7.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building7.b1MaxX) &&
			(building7.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building7.b1MaxZ) &&
			(building7.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building7.b1MaxZ) ||

			(drone.droneMinY <= building8.b1MaxY) &&
			(building8.b1MinX <= drone.droneMinX && drone.droneMinX <= building8.b1MaxX) &&
			(building8.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building8.b1MaxX) &&
			(building8.b1MinZ <= drone.droneMaxZ && drone.droneMaxZ <= building8.b1MaxZ) &&
			(building8.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building8.b1MaxZ))//top
		{
			drone.moveY -= deltaPos.y * 2;
			//cout << "Moving Up" << endl;
		}
	}
	else {

		if (keys['W'])
		{
			drone.speed += drone.droneAcel;
			if (drone.speed >= 0.2f)
			{
				drone.speed = 0.2f;
			}
			drone.rotateX -= drone.speed * 4;
			if (drone.rotateX <= -30.0f)
			{
				drone.rotateX = -30.0f;
			}
			drone.moveZ -= drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveX += drone.speed * sinf(drone.rotateZ * PI / 180);
			//drone.moveY += drone.speed * cosf(drone.rotateY * PI / 180);
			//cout << "Drone Z: " << drone.moveZ << endl;
			//drone.cameraZ += -drone.speed;

		}
		if (keys['S'])
		{
			drone.rotateX += drone.speed * 4;
			if (drone.rotateX >= 30.0f)
			{
				drone.rotateX = 30.0f;
			}
			drone.moveZ += drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveX -= drone.speed * sinf(drone.rotateZ * PI / 180);
			//drone.moveY -= drone.speed * cosf(drone.rotateY * PI / 180);
			//cout << "Drone Z: " << drone.moveZ << endl;
			//drone.cameraZ += drone.speed;

		}
		if (keys['A'] && !drone.isCollide && (drone.moveY > 0))
		{
			drone.rotateY += drone.speed * 4;
			if (drone.rotateY >= 15.0f)
			{
				drone.rotateY = 15.0f;
			}
			drone.moveX -= drone.speed * cosf(drone.rotateZ * PI / 180);
			drone.moveZ -= drone.speed * sinf(drone.rotateZ * PI / 180);
			//cout << "Drone X: " << drone.moveX << endl;
			//drone.cameraX += -drone.speed;

		}
		if (keys['D'] && !drone.isCollide && (drone.moveY > 0))
		{

			drone.rotateY -= drone.speed * 4;
			if (drone.rotateY <= -15.0f)
			{
				drone.rotateY = -15.0f;
			}
			drone.moveX += drone.speed * cosf(drone.rotateZ * PI / 180);;
			drone.moveZ += drone.speed * sinf(drone.rotateZ * PI / 180);
			//cout << "Drone X: " << drone.moveX << endl;
			//drone.cameraX += drone.speed;

		}

		if (keys[VK_SPACE])
		{

			drone.moveY += drone.speed;
			//cout << "Drone Y: " << drone.moveY << endl;
			//drone.cameraY += drone.speed;
		}
		if (keys[VK_SHIFT] && !drone.isCollide && (drone.moveY > 0))
		{
			drone.moveY += -drone.speed;
			//cout << "Drone Y: " << drone.moveY << endl;
			//drone.cameraY += -drone.speed;
		}

	}


	//drone cooldown
	if (!keys['W'] && drone.rotateX <= 0)
	{
		/*drone.speed -= drone.droneDecel;
		if (drone.speed <= 0)
		{
		drone.speed = 0;
		}*/
		drone.rotateX += drone.speed * 4;
		if (drone.rotateX >= 0)
		{
			drone.rotateX = 0;
		}

	}
	if (!keys['S'] && drone.rotateX >= 0)
	{
		/*drone.speed += drone.droneDecel;
		if (drone.speed >= 0)
		{
		drone.speed = 0;
		}*/
		drone.rotateX -= drone.speed * 4;
		if (drone.rotateX <= 0)
		{
			drone.rotateX = 0;
		}

	}
	if (!keys['A'] && drone.rotateY >= 0)
	{
		drone.rotateY -= drone.speed * 4;
		if (drone.rotateY <= 0)
		{
			drone.rotateY = 0;
		}

	}
	if (!keys['D'] && drone.rotateY <= 0)
	{
		drone.rotateY += drone.speed * 4;
		if (drone.rotateY >= 0)
		{
			drone.rotateY = 0;
		}

	}


	//camera keys
	if (keys['1'])
	{
		cameraState = 1;
	}
	if (keys['2'])
	{
		cameraState = 2;
	}
	if (keys['3'])
	{
		cameraState = 3;
	}
	

	//camera keys
	if (keys['1'])
	{
		cameraState = 1;
	}
	if (keys['2'])
	{
		cameraState = 2;
	}
	if (keys['3'])
	{
		cameraState = 3;
	}
	
}


void update()
{
	LARGE_INTEGER t;
	QueryPerformanceCounter(&t);
	__int64 currentTime = t.QuadPart;
	__int64 ticksDone = currentTime - prevTime;
	deltaT = double(ticksDone) * timerFreqRecip;
	prevTime = currentTime;

	/*if (deltaT > 100)
	{
		drone.speed += drone.droneAcelZ;
		
	}*/


	/*drone.moveZ -= drone.speed * cosf(drone.rotateZ * PI / 180);
	drone.moveX += drone.speed * sinf(drone.rotateZ * PI / 180);*/

	currentPos = glm::vec3(drone.moveX,drone.moveY,drone.moveZ);
	deltaPos = currentPos - prevPos;
	if (deltaPos.x != 0)
	{
		glm::normalize(deltaPos.x);
	}
	if (deltaPos.y != 0)
	{
		glm::normalize(deltaPos.y);
	}
	if (deltaPos.z != 0)
	{
		glm::normalize(deltaPos.z);
	}
	prevPos = glm::vec3(drone.moveX, drone.moveY, drone.moveZ);
		
		

	//collision detection

	if ((building1.b1MinX <= drone.droneMinX && drone.droneMinX <= building1.b1MaxX &&
			drone.droneMinY <= building1.b1MaxY &&
			building1.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building1.b1MaxZ) ||

		(building1.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building1.b1MaxX &&
			drone.droneMaxY <= building1.b1MaxY &&
			building1.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building1.b1MaxZ) ||

		(building2.b1MinX <= drone.droneMinX && drone.droneMinX <= building2.b1MaxX &&
			drone.droneMinY <= building2.b1MaxY &&
			building2.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building2.b1MaxZ) ||

		(building2.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building2.b1MaxX &&
			drone.droneMaxY <= building2.b1MaxY &&
			building2.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building2.b1MaxZ) ||

		(building3.b1MinX <= drone.droneMinX && drone.droneMinX <= building3.b1MaxX &&
		drone.droneMinY <= building3.b1MaxY &&
		building3.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building3.b1MaxZ) ||

		(building3.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building3.b1MaxX &&
		drone.droneMaxY <= building3.b1MaxY &&
		building3.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building3.b1MaxZ) ||

		(building4.b1MinX <= drone.droneMinX && drone.droneMinX <= building4.b1MaxX &&
		drone.droneMinY <= building4.b1MaxY &&
		building4.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building4.b1MaxZ) ||

		(building4.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building4.b1MaxX &&
		drone.droneMaxY <= building4.b1MaxY &&
		building4.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building4.b1MaxZ) ||

		(building5.b1MinX <= drone.droneMinX && drone.droneMinX <= building5.b1MaxX &&
		drone.droneMinY <= building5.b1MaxY &&
		building5.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building5.b1MaxZ) ||

		(building5.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building5.b1MaxX &&
		drone.droneMaxY <= building5.b1MaxY &&
		building5.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building5.b1MaxZ) ||


		(building6.b1MinX <= drone.droneMinX && drone.droneMinX <= building6.b1MaxX &&
		drone.droneMinY <= building6.b1MaxY &&
		building6.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building6.b1MaxZ) ||

		(building6.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building6.b1MaxX &&
		drone.droneMaxY <= building6.b1MaxY &&
		building6.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building6.b1MaxZ) ||


		(building7.b1MinX <= drone.droneMinX && drone.droneMinX <= building7.b1MaxX &&
		drone.droneMinY <= building6.b1MaxY &&
		building7.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building7.b1MaxZ) ||

		(building7.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building7.b1MaxX &&
		drone.droneMaxY <= building7.b1MaxY &&
		building7.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building7.b1MaxZ) ||

		(building8.b1MinX <= drone.droneMinX && drone.droneMinX <= building8.b1MaxX &&
		drone.droneMinY <= building8.b1MaxY &&
		building8.b1MinZ <= drone.droneMinZ &&  drone.droneMinZ <= building8.b1MaxZ) ||

		(building8.b1MinX <= drone.droneMaxX && drone.droneMaxX <= building8.b1MaxX &&
		drone.droneMaxY <= building8.b1MaxY &&
		building8.b1MinZ <= drone.droneMaxZ &&  drone.droneMaxZ <= building7.b1MaxZ) ||	
		drone.droneMinY <= 0) 
	{
		drone.isCollide = true;
	}
	else {drone.isCollide = false;}


}
/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	AllocConsole();
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w", stdout);

	// Create Our OpenGL Window
	if (!CreateGLWindow("Drone Simulator",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			processKeys();			//process keyboard
			
			display();					// Draw The Scene
			update();					// update variables
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	
	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);
				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);

	glewInit();

	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		cout << " not possible to make context "<< endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	cout << GLVersionString << endl;

	//OpenGL 3.2 way of checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << OpenGLVersion[0] << " " << OpenGLVersion[1] << endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}



