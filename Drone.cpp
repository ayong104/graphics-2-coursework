#include "Drone.h"


Drone::Drone()
{

}

void Drone::draw(glm::mat4 viewingMatrix, Shader* myShader)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	//glUseProgram(myShader->handle());  // use the shader

	float material_shine[4] = { 0.9f, 0.9f, 0.8f, 1.0f };
	float Material_Ambient[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
	float Material_Diffuse[4] = { 0.8f, 0.8f, 0.5f, 1.0f };
	float Light_Ambient_And_Diffuse[4] = {0.8f, 0.8f, 0.6f, 1.0f};

	//glm::mat4 viewingMatrix = glm::translate(glm::mat4(1.0), glm::vec3(0, 0, -140)); 
	//glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewingMatrix[0][0]); //view matrix 
	light.loadShader(Material_Diffuse, Light_Ambient_And_Diffuse, Material_Ambient, material_shine, 100, myShader);

	glm::mat4 cameraMatrix;


	forwardMovement = glm::vec3(moveX, moveY, moveZ);
	//horizontalMovement = glm::vec3(0, 0, 0);

	prevZ = moveZ;
	prevX = moveX;
	prevY = moveY;

	//DRAW THE MODEL

	//movement variables
	
	//cameraMatrix = glm::lookAt(glm::vec3(0, 0, -140), glm::vec3(moveX, moveY, moveZ), glm::vec3(0, 1, 0));
	


	ModelViewMatrix = glm::mat4(1.0f);
	//viewingMatrix = glm::translate(ModelViewMatrix, glm::vec3(cameraX, cameraY, cameraZ));

	//movement
	
	ModelViewMatrix = glm::translate(viewingMatrix, forwardMovement); //multiply modelviewmatrix by viewingmatrix, does the translation
	//ModelViewMatrix = glm::translate(viewingMatrix, horizontalMovement); //multiply modelviewmatrix by viewingmatrix, does the translation
	ModelViewMatrix = glm::rotate(ModelViewMatrix, 90.0f, glm::vec3(1, 0, 0));//simple transform/rotation to make the models orient in the right position.
	
	//vector3 forward, rotate vector by your value
	//rotation movement
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotateZ, glm::vec3(0, 0, 1));//rotate by z
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotateX, glm::vec3(1, 0, 0));//rotate by x
	ModelViewMatrix = glm::rotate(ModelViewMatrix, rotateY, glm::vec3(0, 1, 0));//rotate by y
	


	



	glUniformMatrix4fv(glGetUniformLocation(myShader->handle(), "ModelViewMatrix"), 1, GL_FALSE, &ModelViewMatrix[0][0]);//passed in

	
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(ModelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(myShader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);
	model.drawElementsUsingVBO(myShader);

	//model.drawOctreeLeaves(myShader);

	droneMinX = model.octree->minX + forwardMovement.x;
	droneMaxX = model.octree->maxX + forwardMovement.x;
	droneMinY = model.octree->minY + forwardMovement.y;
	droneMaxY = model.octree->maxY + forwardMovement.y;	
	droneMinZ = model.octree->minZ + forwardMovement.z;
	droneMaxZ = model.octree->maxZ + forwardMovement.z;

	glFlush();




}
void Drone::loadModel(char* filename, Shader* myShader)
{
	
	
	if (objLoader.loadModel(filename, model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;

		//if you want to translate the object to the origin of the screen,
		//first calculate the centre of the object, then move all the vertices
		//back so that the centre is on the origin.
		//model.calcCentrePoint();
		//model.centreOnZero();


		model.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.


		//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model.initDrawElements();
		model.initVBO(myShader);
		model.deleteVertexFaceData();

	}
	else
	{
		cout << " model failed to load " << endl;
	}
}
	
