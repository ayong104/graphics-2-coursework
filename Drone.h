#pragma once
#include "LightingEngine.h"
#include "shaders\Shader.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"


//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib,"glew32.lib")
#include "glm\glm.hpp"
#include "Octree\Octree.h"



//
class Drone
{
public:
	//model
	glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing
	glm::mat4 ProjectionMatrix; // matrix for the orthographic projection

	//double currentTime = glutGet(GLUT_ELAPSED_TIME);
	glm::vec3 forwardMovement;
	glm::vec3 horizontalMovement;

	
	OBJLoader objLoader;

	//core loads
	ThreeDModel model, modelbox;
	//variables
	float speed = 0.0f;
	//movement
	float moveX = 0.0f;
	float moveY = 3.0f;
	float moveZ = 0.0f;

	float prevX;
	float prevY;
	float prevZ;

	float droneAcel = 0.01f;
	float droneAcelZ = 0.0f;
	float droneAcelY = 0.0f;
	float droneAcelX = 0.0f;
	//rotate
	float rotateX = 0.0f;
	float rotateY = 0.0f;
	float rotateZ = 0.0f;

	LightingEngine light;


	//collider variables
	float droneMinX;
	float droneMaxX;
	float droneMinY;
	float droneMaxY;
	float droneMinZ;
	float droneMaxZ;


	//boolean
	bool isCollide = false;
	bool isCollideXLeft = false;
	bool isCollideXRight = false;
	bool isCollideY = false;
	bool isCollideFloor = false;
	bool isCollideZMax = false;
	bool isCollideZMin = false;

	//methods
	Drone();

	void draw(glm::mat4 viewingMatrix,Shader* myShader);
	void loadModel(char* filename, Shader * myShader);
};