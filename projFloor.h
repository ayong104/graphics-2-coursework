#pragma once
#include "LightingEngine.h"
#include "shaders\Shader.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"


//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "Octree\Octree.h"





//
class projFloor
{
public:
	//model
	glm::mat4 ModelViewMatrix;  // matrix for the modelling and viewing
	glm::mat4 ProjectionMatrix; // matrix for the orthographic projection

	
	OBJLoader objLoader;

	ThreeDModel model, modelbox;
	LightingEngine light;
	//methods
	projFloor();

	void draw(glm::mat4 viewingMatrix, Shader* myShader);;
	void loadModel(char* filename, Shader * myShader);
};